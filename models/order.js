const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema({
	userId: {
			type: String
				},
			
	
	product: [{
		name: {
			type: String,
			required: [true, "Product is required"]
		},
		quantity: {
			type: Number,
			default: 1
		}
	}],


	totalAmount: {
		type: Number,
	},

	purchaseOn: {
		type: Date,
	default: new Date(),
	},




})

									
module.exports = mongoose.model("Order", orderSchema)